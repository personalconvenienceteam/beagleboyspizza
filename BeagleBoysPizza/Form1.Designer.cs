﻿namespace BeagleBoysPizza {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.MainTitleLabel = new System.Windows.Forms.Label();
            this.orderNameTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBoxLabel = new System.Windows.Forms.Label();
            this.basePriceLabel = new System.Windows.Forms.Label();
            this.crustTypeGroupBox = new System.Windows.Forms.GroupBox();
            this.glutenCrustRadioButton = new System.Windows.Forms.RadioButton();
            this.thickCrustRadioButton = new System.Windows.Forms.RadioButton();
            this.thinCrustRadioButton = new System.Windows.Forms.RadioButton();
            this.surchargeLabel = new System.Windows.Forms.Label();
            this.sauceComboBox = new System.Windows.Forms.ComboBox();
            this.sauceComboLabel = new System.Windows.Forms.Label();
            this.toppingsGroupBox = new System.Windows.Forms.GroupBox();
            this.toppingPriceLabel = new System.Windows.Forms.Label();
            this.toppingBox16 = new System.Windows.Forms.CheckBox();
            this.toppingBox15 = new System.Windows.Forms.CheckBox();
            this.toppingBox14 = new System.Windows.Forms.CheckBox();
            this.toppingBox13 = new System.Windows.Forms.CheckBox();
            this.toppingBox12 = new System.Windows.Forms.CheckBox();
            this.toppingBox11 = new System.Windows.Forms.CheckBox();
            this.toppingBox10 = new System.Windows.Forms.CheckBox();
            this.toppingBox9 = new System.Windows.Forms.CheckBox();
            this.toppingBox8 = new System.Windows.Forms.CheckBox();
            this.toppingBox7 = new System.Windows.Forms.CheckBox();
            this.toppingBox6 = new System.Windows.Forms.CheckBox();
            this.toppingBox5 = new System.Windows.Forms.CheckBox();
            this.toppingBox4 = new System.Windows.Forms.CheckBox();
            this.toppingBox3 = new System.Windows.Forms.CheckBox();
            this.toppingBox2 = new System.Windows.Forms.CheckBox();
            this.toppingBox1 = new System.Windows.Forms.CheckBox();
            this.doneButton = new System.Windows.Forms.Button();
            this.crustTypeGroupBox.SuspendLayout();
            this.toppingsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTitleLabel
            // 
            this.MainTitleLabel.AutoSize = true;
            this.MainTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainTitleLabel.Location = new System.Drawing.Point(12, 9);
            this.MainTitleLabel.Name = "MainTitleLabel";
            this.MainTitleLabel.Size = new System.Drawing.Size(351, 52);
            this.MainTitleLabel.TabIndex = 0;
            this.MainTitleLabel.Text = "Welcome To Beagle Boys Pizza\nOrder form";
            // 
            // orderNameTextBox
            // 
            this.orderNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderNameTextBox.Location = new System.Drawing.Point(192, 82);
            this.orderNameTextBox.Name = "orderNameTextBox";
            this.orderNameTextBox.Size = new System.Drawing.Size(214, 20);
            this.orderNameTextBox.TabIndex = 1;
            this.orderNameTextBox.TextChanged += new System.EventHandler(this.orderNameTextBox_TextChanged);
            // 
            // nameTextBoxLabel
            // 
            this.nameTextBoxLabel.AutoSize = true;
            this.nameTextBoxLabel.Location = new System.Drawing.Point(14, 85);
            this.nameTextBoxLabel.Name = "nameTextBoxLabel";
            this.nameTextBoxLabel.Size = new System.Drawing.Size(172, 13);
            this.nameTextBoxLabel.TabIndex = 2;
            this.nameTextBoxLabel.Text = "Please enter a name for your order:";
            // 
            // basePriceLabel
            // 
            this.basePriceLabel.AutoSize = true;
            this.basePriceLabel.Enabled = false;
            this.basePriceLabel.Location = new System.Drawing.Point(14, 113);
            this.basePriceLabel.Name = "basePriceLabel";
            this.basePriceLabel.Size = new System.Drawing.Size(82, 13);
            this.basePriceLabel.TabIndex = 3;
            this.basePriceLabel.Text = "Base Price: $10";
            // 
            // crustTypeGroupBox
            // 
            this.crustTypeGroupBox.BackColor = System.Drawing.SystemColors.Control;
            this.crustTypeGroupBox.Controls.Add(this.glutenCrustRadioButton);
            this.crustTypeGroupBox.Controls.Add(this.thickCrustRadioButton);
            this.crustTypeGroupBox.Controls.Add(this.thinCrustRadioButton);
            this.crustTypeGroupBox.Controls.Add(this.surchargeLabel);
            this.crustTypeGroupBox.Enabled = false;
            this.crustTypeGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crustTypeGroupBox.Location = new System.Drawing.Point(17, 129);
            this.crustTypeGroupBox.Name = "crustTypeGroupBox";
            this.crustTypeGroupBox.Size = new System.Drawing.Size(251, 87);
            this.crustTypeGroupBox.TabIndex = 4;
            this.crustTypeGroupBox.TabStop = false;
            this.crustTypeGroupBox.Text = "Crust";
            // 
            // glutenCrustRadioButton
            // 
            this.glutenCrustRadioButton.AutoSize = true;
            this.glutenCrustRadioButton.Location = new System.Drawing.Point(161, 35);
            this.glutenCrustRadioButton.Name = "glutenCrustRadioButton";
            this.glutenCrustRadioButton.Size = new System.Drawing.Size(84, 17);
            this.glutenCrustRadioButton.TabIndex = 6;
            this.glutenCrustRadioButton.TabStop = true;
            this.glutenCrustRadioButton.Text = "Gluten Free*";
            this.glutenCrustRadioButton.UseVisualStyleBackColor = true;
            this.glutenCrustRadioButton.CheckedChanged += new System.EventHandler(this.crustTypeRadioButton_CheckedChanged);
            // 
            // thickCrustRadioButton
            // 
            this.thickCrustRadioButton.AutoSize = true;
            this.thickCrustRadioButton.Location = new System.Drawing.Point(85, 35);
            this.thickCrustRadioButton.Name = "thickCrustRadioButton";
            this.thickCrustRadioButton.Size = new System.Drawing.Size(52, 17);
            this.thickCrustRadioButton.TabIndex = 5;
            this.thickCrustRadioButton.TabStop = true;
            this.thickCrustRadioButton.Text = "Thick";
            this.thickCrustRadioButton.UseVisualStyleBackColor = true;
            this.thickCrustRadioButton.CheckedChanged += new System.EventHandler(this.crustTypeRadioButton_CheckedChanged);
            // 
            // thinCrustRadioButton
            // 
            this.thinCrustRadioButton.AutoSize = true;
            this.thinCrustRadioButton.Location = new System.Drawing.Point(17, 34);
            this.thinCrustRadioButton.Name = "thinCrustRadioButton";
            this.thinCrustRadioButton.Size = new System.Drawing.Size(46, 17);
            this.thinCrustRadioButton.TabIndex = 4;
            this.thinCrustRadioButton.TabStop = true;
            this.thinCrustRadioButton.Text = "Thin";
            this.thinCrustRadioButton.UseVisualStyleBackColor = true;
            this.thinCrustRadioButton.CheckedChanged += new System.EventHandler(this.crustTypeRadioButton_CheckedChanged);
            // 
            // surchargeLabel
            // 
            this.surchargeLabel.AutoSize = true;
            this.surchargeLabel.Location = new System.Drawing.Point(53, 71);
            this.surchargeLabel.Name = "surchargeLabel";
            this.surchargeLabel.Size = new System.Drawing.Size(107, 13);
            this.surchargeLabel.TabIndex = 3;
            this.surchargeLabel.Text = "*Incurs $2 Surcharge";
            // 
            // sauceComboBox
            // 
            this.sauceComboBox.Enabled = false;
            this.sauceComboBox.FormattingEnabled = true;
            this.sauceComboBox.Items.AddRange(new object[] {
            "Tomato",
            "Barbeque"});
            this.sauceComboBox.Location = new System.Drawing.Point(312, 166);
            this.sauceComboBox.Name = "sauceComboBox";
            this.sauceComboBox.Size = new System.Drawing.Size(239, 21);
            this.sauceComboBox.TabIndex = 5;
            this.sauceComboBox.SelectedIndexChanged += new System.EventHandler(this.sauceComboBox_SelectedIndexChanged);
            // 
            // sauceComboLabel
            // 
            this.sauceComboLabel.AutoSize = true;
            this.sauceComboLabel.Enabled = false;
            this.sauceComboLabel.Location = new System.Drawing.Point(311, 150);
            this.sauceComboLabel.Name = "sauceComboLabel";
            this.sauceComboLabel.Size = new System.Drawing.Size(95, 13);
            this.sauceComboLabel.TabIndex = 6;
            this.sauceComboLabel.Text = "Select your sauce:";
            // 
            // toppingsGroupBox
            // 
            this.toppingsGroupBox.Controls.Add(this.toppingPriceLabel);
            this.toppingsGroupBox.Controls.Add(this.toppingBox16);
            this.toppingsGroupBox.Controls.Add(this.toppingBox15);
            this.toppingsGroupBox.Controls.Add(this.toppingBox14);
            this.toppingsGroupBox.Controls.Add(this.toppingBox13);
            this.toppingsGroupBox.Controls.Add(this.toppingBox12);
            this.toppingsGroupBox.Controls.Add(this.toppingBox11);
            this.toppingsGroupBox.Controls.Add(this.toppingBox10);
            this.toppingsGroupBox.Controls.Add(this.toppingBox9);
            this.toppingsGroupBox.Controls.Add(this.toppingBox8);
            this.toppingsGroupBox.Controls.Add(this.toppingBox7);
            this.toppingsGroupBox.Controls.Add(this.toppingBox6);
            this.toppingsGroupBox.Controls.Add(this.toppingBox5);
            this.toppingsGroupBox.Controls.Add(this.toppingBox4);
            this.toppingsGroupBox.Controls.Add(this.toppingBox3);
            this.toppingsGroupBox.Controls.Add(this.toppingBox2);
            this.toppingsGroupBox.Controls.Add(this.toppingBox1);
            this.toppingsGroupBox.Enabled = false;
            this.toppingsGroupBox.Location = new System.Drawing.Point(16, 231);
            this.toppingsGroupBox.Name = "toppingsGroupBox";
            this.toppingsGroupBox.Size = new System.Drawing.Size(684, 159);
            this.toppingsGroupBox.TabIndex = 7;
            this.toppingsGroupBox.TabStop = false;
            this.toppingsGroupBox.Text = "Toppings";
            // 
            // toppingPriceLabel
            // 
            this.toppingPriceLabel.AutoSize = true;
            this.toppingPriceLabel.Location = new System.Drawing.Point(124, 134);
            this.toppingPriceLabel.Name = "toppingPriceLabel";
            this.toppingPriceLabel.Size = new System.Drawing.Size(356, 13);
            this.toppingPriceLabel.TabIndex = 16;
            this.toppingPriceLabel.Text = "Choose your toppings. The first 4 are free, after that add $1.00 per topping";
            // 
            // toppingBox16
            // 
            this.toppingBox16.AutoSize = true;
            this.toppingBox16.Location = new System.Drawing.Point(449, 103);
            this.toppingBox16.Name = "toppingBox16";
            this.toppingBox16.Size = new System.Drawing.Size(66, 17);
            this.toppingBox16.TabIndex = 15;
            this.toppingBox16.Text = "Cheddar";
            this.toppingBox16.UseVisualStyleBackColor = true;
            this.toppingBox16.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox15
            // 
            this.toppingBox15.AutoSize = true;
            this.toppingBox15.Location = new System.Drawing.Point(449, 78);
            this.toppingBox15.Name = "toppingBox15";
            this.toppingBox15.Size = new System.Drawing.Size(57, 17);
            this.toppingBox15.TabIndex = 14;
            this.toppingBox15.Text = "Bacon";
            this.toppingBox15.UseVisualStyleBackColor = true;
            this.toppingBox15.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox14
            // 
            this.toppingBox14.AutoSize = true;
            this.toppingBox14.Location = new System.Drawing.Point(449, 55);
            this.toppingBox14.Name = "toppingBox14";
            this.toppingBox14.Size = new System.Drawing.Size(73, 17);
            this.toppingBox14.TabIndex = 13;
            this.toppingBox14.Text = "Pineapple";
            this.toppingBox14.UseVisualStyleBackColor = true;
            this.toppingBox14.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox13
            // 
            this.toppingBox13.AutoSize = true;
            this.toppingBox13.Location = new System.Drawing.Point(449, 31);
            this.toppingBox13.Name = "toppingBox13";
            this.toppingBox13.Size = new System.Drawing.Size(72, 17);
            this.toppingBox13.TabIndex = 12;
            this.toppingBox13.Text = "Capsicum";
            this.toppingBox13.UseVisualStyleBackColor = true;
            this.toppingBox13.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox12
            // 
            this.toppingBox12.AutoSize = true;
            this.toppingBox12.Location = new System.Drawing.Point(309, 79);
            this.toppingBox12.Name = "toppingBox12";
            this.toppingBox12.Size = new System.Drawing.Size(75, 17);
            this.toppingBox12.TabIndex = 11;
            this.toppingBox12.Text = "Mushroom";
            this.toppingBox12.UseVisualStyleBackColor = true;
            this.toppingBox12.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox11
            // 
            this.toppingBox11.AutoSize = true;
            this.toppingBox11.Location = new System.Drawing.Point(309, 103);
            this.toppingBox11.Name = "toppingBox11";
            this.toppingBox11.Size = new System.Drawing.Size(47, 17);
            this.toppingBox11.TabIndex = 10;
            this.toppingBox11.Text = "Feta";
            this.toppingBox11.UseVisualStyleBackColor = true;
            this.toppingBox11.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox10
            // 
            this.toppingBox10.AutoSize = true;
            this.toppingBox10.Location = new System.Drawing.Point(309, 55);
            this.toppingBox10.Name = "toppingBox10";
            this.toppingBox10.Size = new System.Drawing.Size(65, 17);
            this.toppingBox10.TabIndex = 9;
            this.toppingBox10.Text = "Spinach";
            this.toppingBox10.UseVisualStyleBackColor = true;
            this.toppingBox10.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox9
            // 
            this.toppingBox9.AutoSize = true;
            this.toppingBox9.Location = new System.Drawing.Point(309, 31);
            this.toppingBox9.Name = "toppingBox9";
            this.toppingBox9.Size = new System.Drawing.Size(74, 17);
            this.toppingBox9.TabIndex = 8;
            this.toppingBox9.Text = "Pepperoni";
            this.toppingBox9.UseVisualStyleBackColor = true;
            this.toppingBox9.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox8
            // 
            this.toppingBox8.AutoSize = true;
            this.toppingBox8.Location = new System.Drawing.Point(176, 103);
            this.toppingBox8.Name = "toppingBox8";
            this.toppingBox8.Size = new System.Drawing.Size(71, 17);
            this.toppingBox8.TabIndex = 7;
            this.toppingBox8.Text = "Mozarella";
            this.toppingBox8.UseVisualStyleBackColor = true;
            this.toppingBox8.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox7
            // 
            this.toppingBox7.AutoSize = true;
            this.toppingBox7.Location = new System.Drawing.Point(176, 79);
            this.toppingBox7.Name = "toppingBox7";
            this.toppingBox7.Size = new System.Drawing.Size(80, 17);
            this.toppingBox7.TabIndex = 6;
            this.toppingBox7.Text = "Beef Mince";
            this.toppingBox7.UseVisualStyleBackColor = true;
            this.toppingBox7.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox6
            // 
            this.toppingBox6.AutoSize = true;
            this.toppingBox6.Location = new System.Drawing.Point(176, 55);
            this.toppingBox6.Name = "toppingBox6";
            this.toppingBox6.Size = new System.Drawing.Size(48, 17);
            this.toppingBox6.TabIndex = 5;
            this.toppingBox6.Text = "Ham";
            this.toppingBox6.UseVisualStyleBackColor = true;
            this.toppingBox6.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox5
            // 
            this.toppingBox5.AutoSize = true;
            this.toppingBox5.Location = new System.Drawing.Point(176, 31);
            this.toppingBox5.Name = "toppingBox5";
            this.toppingBox5.Size = new System.Drawing.Size(55, 17);
            this.toppingBox5.TabIndex = 4;
            this.toppingBox5.Text = "Olives";
            this.toppingBox5.UseVisualStyleBackColor = true;
            this.toppingBox5.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox4
            // 
            this.toppingBox4.AutoSize = true;
            this.toppingBox4.Location = new System.Drawing.Point(41, 103);
            this.toppingBox4.Name = "toppingBox4";
            this.toppingBox4.Size = new System.Drawing.Size(56, 17);
            this.toppingBox4.TabIndex = 3;
            this.toppingBox4.Text = "Prawn";
            this.toppingBox4.UseVisualStyleBackColor = true;
            this.toppingBox4.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox3
            // 
            this.toppingBox3.AutoSize = true;
            this.toppingBox3.Location = new System.Drawing.Point(41, 79);
            this.toppingBox3.Name = "toppingBox3";
            this.toppingBox3.Size = new System.Drawing.Size(65, 17);
            this.toppingBox3.TabIndex = 2;
            this.toppingBox3.Text = "Chicken";
            this.toppingBox3.UseVisualStyleBackColor = true;
            this.toppingBox3.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox2
            // 
            this.toppingBox2.AutoSize = true;
            this.toppingBox2.Location = new System.Drawing.Point(41, 55);
            this.toppingBox2.Name = "toppingBox2";
            this.toppingBox2.Size = new System.Drawing.Size(69, 17);
            this.toppingBox2.TabIndex = 1;
            this.toppingBox2.Text = "Jalapeno";
            this.toppingBox2.UseVisualStyleBackColor = true;
            this.toppingBox2.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // toppingBox1
            // 
            this.toppingBox1.AutoSize = true;
            this.toppingBox1.Location = new System.Drawing.Point(41, 31);
            this.toppingBox1.Name = "toppingBox1";
            this.toppingBox1.Size = new System.Drawing.Size(73, 17);
            this.toppingBox1.TabIndex = 0;
            this.toppingBox1.Text = "Tomatoes";
            this.toppingBox1.UseVisualStyleBackColor = true;
            this.toppingBox1.Click += new System.EventHandler(this.toppingBox_Click);
            // 
            // doneButton
            // 
            this.doneButton.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.doneButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.doneButton.Location = new System.Drawing.Point(312, 408);
            this.doneButton.Name = "doneButton";
            this.doneButton.Size = new System.Drawing.Size(75, 23);
            this.doneButton.TabIndex = 8;
            this.doneButton.Text = "Done";
            this.doneButton.UseVisualStyleBackColor = false;
            this.doneButton.Visible = false;
            this.doneButton.Click += new System.EventHandler(this.doneButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(726, 452);
            this.Controls.Add(this.doneButton);
            this.Controls.Add(this.toppingsGroupBox);
            this.Controls.Add(this.sauceComboLabel);
            this.Controls.Add(this.sauceComboBox);
            this.Controls.Add(this.basePriceLabel);
            this.Controls.Add(this.crustTypeGroupBox);
            this.Controls.Add(this.nameTextBoxLabel);
            this.Controls.Add(this.orderNameTextBox);
            this.Controls.Add(this.MainTitleLabel);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Name = "Form1";
            this.Text = "Beagle Boys";
            this.crustTypeGroupBox.ResumeLayout(false);
            this.crustTypeGroupBox.PerformLayout();
            this.toppingsGroupBox.ResumeLayout(false);
            this.toppingsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MainTitleLabel;
        private System.Windows.Forms.TextBox orderNameTextBox;
        private System.Windows.Forms.Label nameTextBoxLabel;
        private System.Windows.Forms.Label basePriceLabel;
        private System.Windows.Forms.GroupBox crustTypeGroupBox;
        private System.Windows.Forms.Label surchargeLabel;
        private System.Windows.Forms.ComboBox sauceComboBox;
        private System.Windows.Forms.Label sauceComboLabel;
        private System.Windows.Forms.GroupBox toppingsGroupBox;
        private System.Windows.Forms.Label toppingPriceLabel;
        private System.Windows.Forms.CheckBox toppingBox16;
        private System.Windows.Forms.CheckBox toppingBox15;
        private System.Windows.Forms.CheckBox toppingBox14;
        private System.Windows.Forms.CheckBox toppingBox13;
        private System.Windows.Forms.CheckBox toppingBox12;
        private System.Windows.Forms.CheckBox toppingBox11;
        private System.Windows.Forms.CheckBox toppingBox10;
        private System.Windows.Forms.CheckBox toppingBox9;
        private System.Windows.Forms.CheckBox toppingBox8;
        private System.Windows.Forms.CheckBox toppingBox7;
        private System.Windows.Forms.CheckBox toppingBox6;
        private System.Windows.Forms.CheckBox toppingBox5;
        private System.Windows.Forms.CheckBox toppingBox4;
        private System.Windows.Forms.CheckBox toppingBox3;
        private System.Windows.Forms.CheckBox toppingBox2;
        private System.Windows.Forms.CheckBox toppingBox1;
        private System.Windows.Forms.Button doneButton;
        private System.Windows.Forms.RadioButton glutenCrustRadioButton;
        private System.Windows.Forms.RadioButton thickCrustRadioButton;
        private System.Windows.Forms.RadioButton thinCrustRadioButton;
    }
}

