﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace BeagleBoysPizza {
    /// <summary>
    /// Form1 is a pizza order form. The user enters a name, selects a crust type, sauce and a number of
    /// toppings. The user then press a button and is presented with a message box presenting a description
    /// of their order and a price.
    /// 
    /// Author: Roderick Lenz, N9438157
    /// Date: September, 2016
    /// </summary>
    
    public partial class Form1 : Form {
        const int BASE_PRICE = 10;
        const int GLUTEN_SURCHARGE = 2;
        const int FREE_TOPPINGS = 4;
        enum CrustType { Thin, Thick, Gluten_Free }
        CrustType selectedCrust;
        int numberOfToppings = 0;
        string enteredName, selectedSauce;



        public Form1() {
            InitializeComponent();
        }// end Form1



        private void orderNameTextBox_TextChanged(object sender, EventArgs e) {

            enteredName = orderNameTextBox.Text;

            // enable crust selection
            basePriceLabel.Enabled = true;
            crustTypeGroupBox.Enabled = true;

        }// end orderNameTextBox_TextChanged



        private void crustTypeRadioButton_CheckedChanged(object sender, EventArgs e) {

            // Enable sauce selection
            sauceComboLabel.Enabled = true;
            sauceComboBox.Enabled = true;

            // save the crust type
            if (thinCrustRadioButton.Checked) {
                selectedCrust = CrustType.Thin;
            } else if (thickCrustRadioButton.Checked) {
                selectedCrust = CrustType.Thick;
            } else if (glutenCrustRadioButton.Checked) {
                selectedCrust = CrustType.Gluten_Free;
            }// end if else

        }// end crustTypeRadioButton_CheckedChanged



        private void sauceComboBox_SelectedIndexChanged(object sender, EventArgs e) {

            selectedSauce = sauceComboBox.SelectedItem.ToString();
            toppingsGroupBox.Enabled = true;

        }// end sauceComboBox_SelectedIndexChanged



        private void toppingBox_Click(object sender, EventArgs e) {
            CheckBox checkedState = (CheckBox)sender;
            doneButton.Visible = true;

            CalculateNumberOfToppings(checkedState);

        }// end toppingBox_Click



        private void doneButton_Click(object sender, EventArgs e) {
            int pizzaPrice;

            pizzaPrice = CalculatePizzaPrice();

            if (pizzaPrice == 0) {
                return;
            }// end if

            ShowOrderMessageBox(pizzaPrice);
            ShowFinalMessageBox();
            
        }// end doneButton_Click


        /// <summary>
        /// CalculateNumberOfToppings either increments the number of toppings if a box is checked or decrements
        /// it if a box is unchecked
        /// </summary>
        /// <param name="checkedState">CheckBox state</param>
        private void CalculateNumberOfToppings(CheckBox checkedState) {

            if (checkedState.Checked) {
                numberOfToppings++;
            } else {
                numberOfToppings--;
            }// end if else

        }// end CalculateNumberOfToppings


        /// <summary>
        /// Calculates the price of the pizza when the done button is pressed
        /// </summary>
        /// <returns>The pizza price</returns>
        private int CalculatePizzaPrice() {
            int totalPrice = BASE_PRICE;

            // check to see if more than one topping is selected, if not show a message box and return
            // 0. Otherwise calculate the pizza price based on base price and number of free toppings 
            if (numberOfToppings == 0) {
                MessageBox.Show("Please Select at least 1 topping\n"
                                + " Remember the first 4 are free");
                return 0;
            }else if (numberOfToppings > FREE_TOPPINGS) {
                totalPrice += numberOfToppings - FREE_TOPPINGS;
            }// end if else
            
            // Add surcharge if gluten free base is selected
            if (selectedCrust == CrustType.Gluten_Free) {
                totalPrice += GLUTEN_SURCHARGE;
            }// end if

            return totalPrice;
        }// end CalculatePizzaPrice


        /// <summary>
        /// Show a message box with the name of the person, description of the pizza and total price
        /// </summary>
        /// <param name="pizzaPrice">price of the pizza</param>
        private void ShowOrderMessageBox(int pizzaPrice) {
            string orderMessage, crustOutput;

            // Convert crust type to a string so message will display "Gluten free" and not "Gluten_Free" if needed
            if (selectedCrust== CrustType.Gluten_Free) {
                crustOutput = "Gluten free";
            }else {
                crustOutput = selectedCrust.ToString();
            }// end if else

            // Create string for order message
            orderMessage = String.Format("Thank you for your order {0}\n"
                + "You ordered a {1} pizza with {2} sauce and {3} toppings\n\n"
                + "Your total is {4:c}", enteredName, crustOutput, selectedSauce, numberOfToppings, pizzaPrice);

            MessageBox.Show(orderMessage);
        }// end ShowOrderMessage


        /// <summary>
        /// Show the goodbye message and close program
        /// </summary>
        private void ShowFinalMessageBox() {

            string goodbyeMessage = "Thanks for Choosing Beagle Boys Pizza\n"
                            + "Your pizza will be with you shortly!";
            MessageBox.Show(goodbyeMessage);
            Close();

        }// end ShowFinalMessageBox()

    }
}
